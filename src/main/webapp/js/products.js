function loadData() {
  let request = sendRequest("producto/listar", "GET", "");
  request.onload = function () {
    let table = document.getElementById("products-table");
    table.innerHTML = "";
    let data = request.response;
    data.forEach((element) => {
      table.innerHTML += `
                  <tr>
                      <th>${element.id}</th>
                      <td>${element.name}</td>
                      <td>${element.price}</td>
                      <td>${element.createdAt}</td>
                      <td>${element.updateAt ? element.updateAt : ""}</td>
                      <td>
                          <button type="button" class="btn btn-primary" onclick='window.location = "form_products.html?id=${
                            element.id
                          }"'>Editar</button>
                          <button type="button" class="btn btn-danger" onclick='deleteProducto(${
                            element.id
                          })'>Eliminar</button>
                      </td>
                  </tr>
  
                  `;
    });
  };
}

function loadProducto(idProducto) {
  let request = sendRequest("producto/" + idProducto, "GET", "");
  request.onload = function () {
    let name = document.getElementById("product-name");
    let price = document.getElementById("product-price");
    let id = document.getElementById("product-id");

    let product = request.response;
    id.value = product.id;
    name.value = product.name;
    price.value = product.price;
  };
}

function deleteProducto(idProducto) {
  const request = sendRequest("producto/eliminar/" + idProducto, "DELETE", "");
  request.onload = function () {
    loadData();
  };
}

function saveProducto() {
  let name = document.getElementById("product-name").value;
  let sale = document.getElementById("product-price").value;
  let id = document.getElementById("product-id").value;
  let data = {
    id: id,
    name: name,
    price: sale
  };
  let request = id
    ? sendRequest("producto/actualizar", "PUT", data)
    : sendRequest("producto/agregar", "POST", data);

  request.onload = function () {
    window.location = "products.html";
  };
}
