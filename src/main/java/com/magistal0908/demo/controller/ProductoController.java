/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.magistal0908.demo.controller;

import com.magistal0908.demo.dao.ProductoDao;
import com.magistal0908.demo.model.Producto;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ingdeiver
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private ProductoDao productoDao;

    @GetMapping("/listar")
    public Iterable<Producto> findAll() {
        return productoDao.findAll();
    }

    @DeleteMapping(value = "/eliminar/{id}")
    public ResponseEntity<Producto> delete(@PathVariable(name = "id") Integer id) {
        Optional<Producto> pEncontrado = productoDao.findById(id);
        if (pEncontrado.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            productoDao.deleteById(id);
            return new ResponseEntity<>(pEncontrado.get(), HttpStatus.OK);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Producto> findOneById(@PathVariable(name = "id") Integer id) {
        Optional<Producto> pEncontrado = productoDao.findById(id);
        if (pEncontrado.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(pEncontrado.get(), HttpStatus.OK);
        }
    }

    @PostMapping("/agregar")
    public ResponseEntity<Producto> save(@RequestBody Producto producto) {
        Producto pGuardado = productoDao.save(producto);
        return new ResponseEntity<>(pGuardado, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<Producto> update(@RequestBody Producto producto) {
        // se comprueba si existe el producto a actualizar
        Optional<Producto> pEncontrado = productoDao.findById(producto.getId());
        if (pEncontrado.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // Si el objeto producto tiene un idProducto, entonces actualiza el producto con ese id
        Producto pActualizado = productoDao.save(producto);
        return new ResponseEntity<>(pActualizado, HttpStatus.OK);
    }

}
