/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.magistal0908.demo.dao;

import com.magistal0908.demo.model.Producto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ingdeiver
 */
@Repository
public interface ProductoDao extends CrudRepository<Producto, Integer> {
    
}
