package com.magistal0908.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Magistal0908Application {

	public static void main(String[] args) {
		SpringApplication.run(Magistal0908Application.class, args);
	}

}
